Name:           xwayland-run
Version:        0.0.4
Release:        5%{?dist}
Summary:        Set of utilities to run headless X/Wayland clients

License:        GPL-2.0-or-later
URL:            https://gitlab.freedesktop.org/ofourdan/xwayland-run
Source0:        %{url}/-/archive/%{version}/%{name}-%{version}.tar.bz2

# https://gitlab.freedesktop.org/ofourdan/xwayland-run/-/merge_requests/19
Patch1: 0001-wlheadless-Ignore-os.waitpid-1-0-error.patch
# https://gitlab.freedesktop.org/ofourdan/xwayland-run/-/merge_requests/21
Patch2: 0001-wlheadless-Set-sensible-defaults-for-XDG-dirs.patch
# https://gitlab.freedesktop.org/ofourdan/xwayland-run/-/merge_requests/22
Patch3: 0002-wlheadless-Wait-for-the-compositor-up-to-150-secs.patch

BuildArch:      noarch

BuildRequires:  meson >= 0.60.0
BuildRequires:  git-core
BuildRequires:  python3-devel
Requires:       (weston or cage or kwin-wayland or mutter or gnome-kiosk)
Requires:       xorg-x11-server-Xwayland
Requires:       xorg-x11-xauth
Requires:       dbus-daemon
Suggests:       mutter

# Provide names of the other utilities included
Provides:       wlheadless-run = %{version}-%{release}
Provides:       xwfb-run = %{version}-%{release}

%description
xwayland-run contains a set of small utilities revolving around running
Xwayland and various Wayland compositor headless.


%prep
%autosetup -S git_am


%build
%meson -Dcompositor=mutter
%meson_build


%install
%meson_install


%files
%license COPYING
%doc README.md
%{_bindir}/wlheadless-run
%{_bindir}/xwayland-run
%{_bindir}/xwfb-run
%{_datadir}/wlheadless/
%{_mandir}/man1/wlheadless-run.1*
%{_mandir}/man1/xwayland-run.1*
%{_mandir}/man1/xwfb-run.1*
%{python3_sitelib}/wlheadless/


%changelog
* Thu Dec 12 2024 Olivier Fourdan <ofourdan@redhat.com> - 0.0.4-5
- Fix typo in version number (RHEL-60056)

* Thu Dec 12 2024 Olivier Fourdan <ofourdan@redhat.com> - 0.0.4-4
- Backport fix for XDG dirs and compositor timeout (RHEL-60056)

* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 0.0.4-3
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Tue Jul 09 2024 Olivier Fourdan <ofourdan@redhat.com> - 0.0.4-2
- Backport fix for waitpid errors (RHEL-46355)
- Suggest mutter since this is the default compositor

* Mon Jul  1 2024 Olivier Fourdan <ofourdan@redhat.com> - 0.0.4-1
- Update to 0.0.4
- Require xorg-x11-xauth and dbus-daemon
- Use mutter as default compositor if none is specified

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 0.0.2-5
- Bump release for June 2024 mass rebuild

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 0.0.2-5
- Bump release for June 2024 mass rebuild

* Sat Jan 27 2024 Fedora Release Engineering <releng@fedoraproject.org> - 0.0.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Sat Jan 13 2024 Neal Gompa <ngompa@fedoraproject.org> - 0.0.2-3
- Refresh kwin support patch with final version

* Sun Dec 10 2023 Neal Gompa <ngompa@fedoraproject.org> - 0.0.2-2
- Refresh kwin support patch
- Add provides for other included utilities

* Sun Dec 10 2023 Neal Gompa <ngompa@fedoraproject.org> - 0.0.2-1
- Initial package
